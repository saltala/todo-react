import React, { Component } from 'react';

const Context = React.createContext();

export class Provider extends Component {
    state = {
        taskInput: '',
        todos: [],
        addTask: (todo) => {
            this.setState({ todos: [todo, ...this.state.todos] })
        },
        deleteTask: (id) => {
            this.setState({ todos: [...this.state.todos.filter(todo => todo.id !== id)] })
        }
    }

    render() {
        return (
            <Context.Provider value={this.state}>
                {this.props.children}
            </Context.Provider>
        )
    }
}

export const Consumer = Context.Consumer;