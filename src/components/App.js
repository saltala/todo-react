import React from 'react';
import TaskInput from './TaskInput';
import List from './List';

import { Provider } from '../context/context';

class App extends React.Component {
    render() {
        return (
            <Provider>
                <div className="ui container App">
                    <TaskInput />
                    <List />
                </div>
            </Provider>
        )
    }
}

export default App;