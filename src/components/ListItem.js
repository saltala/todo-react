import React, { Fragment, Component } from 'react'
import { Consumer } from '../context/context';
import config from '../utils/config';
import axios from 'axios';

class ListItem extends Component {
    state = {
        photo: '',
    }

    componentDidMount() {
        this.getImage();
    }

    getImage = async () => {
        const { API_URI, UNSPLASH_ACCESS } = config;

        const image = await axios.get(`${API_URI}/photos/random/?client_id=${UNSPLASH_ACCESS}`);

        this.setState({ photo: image.data.links.download });
    }

    handleClick = (deleteTask, id) => {
        deleteTask(id);
    }

    render() {
        return (
            <Consumer>
                {({ deleteTask }) => (
                    <Fragment>
                        <div className="item">
                            <div className="right floated content">
                                <div className="ui button" onClick={(e) => this.handleClick(deleteTask, this.props.id)} >Done</div>
                            </div>
                            <img className="ui avatar image" src={this.state.photo} alt="Todo Avatar"></img>
                            <div className="content">
                                {this.props.title}
                            </div>
                        </div>
                    </Fragment >
                )}

            </Consumer>
        )
    }
}

// TODO: <img className="ui avatar image" src="/images/avatar2/small/lena.png" alt="Avatar" />
// Get random image from unsplash API




export default ListItem;