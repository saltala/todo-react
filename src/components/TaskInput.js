import React, { Component } from 'react'
import uuid from 'uuid';
import { Consumer } from '../context/context'

class TaskInput extends Component {
    state = {
        task: '',
    }

    handleInputChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    handleFormSubmit = (event, addTask) => {
        event.preventDefault();

        const task = {
            id: uuid(),
            title: this.state.task,
        };

        addTask(task);

        this.setState({ task: '' });
    }

    render() {
        return (
            <Consumer>
                {({ addTask }) => (
                    <div>
                        <form onSubmit={(e) => this.handleFormSubmit(e, addTask)}>
                            <div className="ui fluid action input" style={{ marginTop: '2rem', marginBottom: '2rem' }} >
                                <input type="text" placeholder="Enter your task..." value={this.state.task} name="task" onChange={this.handleInputChange} autoComplete="off" />
                            </div>
                        </form>
                    </div>
                )}
            </Consumer>
        )
    }
}


export default TaskInput;
