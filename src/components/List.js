import React, { Component } from 'react'
import ListItem from '../components/ListItem';

import { Consumer } from '../context/context';

class List extends Component {
    renderItems = (todos) => {
        return todos.map((todo, index) => (
            <ListItem key={index} id={todo.id} title={todo.title} />
        ));
    }

    render() {
        return (
            <Consumer>
                {({ todos }) => (
                    <div>
                        <div className="ui middle aligned divided list">
                            {this.renderItems(todos)}
                        </div>
                    </div>
                )}
            </Consumer>
        )
    }
}

export default List;
